# 研究基础技能训练

研究新手训练手册

# 说在前面的话

* 研究生期间如何做研究：一些建议 <https://blog.csdn.net/chieryu/article/details/54602331> <http://www.ece.rutgers.edu/~pompili/index_file/extra/HowToDoResearch_ANRG_WP02001.pdf> [本地下载](HowToDoResearch_ANRG_WP02001.pdf)
* 写给新生：研究生需要完成的九项任务 <http://yz.chsi.com.cn/kyzx/jyxd/201508/20150826/1503385018.html>
* 研究生的学习计划 <https://www.ruiwen.com/xuexijihua/1176788.html>

# 我们的目标

1. 不仅能实现，还要勤思考；
2. 不仅能交账，还要善表述；
3. 不仅能模仿，更要敢探索。

怎么做？

1. 学会欣赏论文的"美"，新颖在何处？怎样一路思考及此？如何精炼、全面刻画？
2. 如何实现那种"美"？基本功：讨论、写作、渲染，奇技淫巧VS结硬寨,打呆仗
3. 那么一点点运气，边界处的未知

# 阅读

## 基础工具

1. 文献管理 Zotero，替代品 NoteExpress、EndNote
2. 脉络整理 Freemind，替代品 XMind
3. 笔记管理 _markdown+git\/rsync_，各类在线笔记

练习

1. 文献入库，通过学校图书馆页面，配合浏览器插件
2. 元数据补齐，其他资料（胶片）补齐，标签分类

然后从列表开始，制定阅读计划，思考如何起步？关注哪些方面？

## 泛读

聚焦于摘要，从几百字中勾勒出下列关键描述：

1. 提出问题，即应用背景，价值所在，应回答"为什么值得关注"、"哪里最为困难"这2个问题，分别形成大背景、小背景
2. 关键贡献，即作者主要方法技巧，最有特色之处，应回答"创新从何处体现"这个问题，正确理解文意后，应感受到激励
3. 实验效果，即作者如何举证，怎样罗列、渲染数据，组织逻辑，给出全面证据，应回答"客观效果如何"这个问题

## 精读

聚焦于相关工作、关键设计、测试方法与数据3项主要内容，回答下述问题：

1. 在所研究背景中，有哪些同样值得关注的成果，仍存在哪些共性问题？为本项研究提出具体目标
2. 作者如何将关键思想方法归纳成为抽象算法、理论模型与公式，或进一步加以数学证明
3. 作者怎样设计实验
    * 如何构造实验环境，软硬件细节描述；
    * 又怎样设计实验案例，各项有关参数调整，观测范围设置；
    * 如何归纳数据变化趋势、涨落比较，从中建立成套逻辑，为贡献提供佐证

### 论文学习经验谈

1. [How to read a research paper --- Harvard](https://www.eecs.harvard.edu/~michaelm/postscripts/ReadPaper.pdf), [Local Link](./harvard-readpaper.pdf)
2. [How to Read a Paper --- Waterloo](http://blizzard.cs.uwaterloo.ca/keshav/home/Papers/data/07/paper-reading.pdf),[Local Link](./waterloo-paper-reading.pdf)
3. [如何阅读学术研究论文？--- B站传送门](https://www.bilibili.com/video/av25584054)

# 构思

## 基础工具

1. 学校正版 Microsoft Office Visio
2. 开源免费 LibreOffice Draw

## 基本原则

符号标记最简，信息量最大，充分避免歧义

参考样式

检验一下

1. 结构描述
2. 流程描述

# 动手

综合入门 <https://github.com/learnbyexample>

## Linux基础

1. 命令行工具 <https://github.com/learnbyexample/Linux_command_line>
2. 命令行文本处理 <https://github.com/learnbyexample/Command-line-text-processing>
3. 文本编辑器vim <https://github.com/learnbyexample/vim_reference>
4. 版本管理器git <http://rogerdudler.github.io/git-guide/index.zh.html> <https://github.com/cs-course/git-tutorial>
5. 窗口复用器tmux <https://github.com/cs-course/tmux-tutorial>

检验一下:

1. 写一个用随机数填充的文本文件，每行两个整数用空格隔开
2. 统计行数、查找特定数字、前缀、组合
3. 排序、不同数字计数

## Python基础

1. 入门教程 <https://github.com/learnbyexample/Python_Basics>
2. 交互式在线界面 Python Notebook <https://github.com/cs-course/jupyter-tutorial>

# 分析

## 基础工具

1. 与实验代码联动 Matplotlib
2. 与Excel手感一致 Origin
3. 一体化计算仿真平台 Matlab

## 数据图表

# 论述

原则：熟思、精辟、逻辑严谨、不要迷信任何工具

## 习得方法

1. **_面对面讨论_**
2. **_面对面讨论_**
3. **_面对面讨论_**

Zhan @ HUST 2018

